<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'DevicesController@create')->name('devices.create');
Route::post('/', 'DevicesController@store')->name('devices.store');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/admin', 'Admin\AdminController@index')->name('admin.admin.index');
});