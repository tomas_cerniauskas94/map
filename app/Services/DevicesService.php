<?php

namespace App\Service;

class DevicesService
{
    public function getPlaceInfoByCoords($lat, $long)
    {
        $opts = array('http' => array('header' => "User-Agent: StevesCleverAddressScript 3.7.6\r\n"));
        $context = stream_context_create($opts);

        $result = file_get_contents('https://nominatim.openstreetmap.org/reverse?lat=' . $lat . '&lon=' . $long . '&format=json', false, $context);

        return json_decode($result, true);
    }

    public function distanceBetweenTwoPoints($lat1, $lon1, $lat2, $lon2, $unit)
    {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function getDistanceBettweenDevices($devices)
    {
        $distanceBettweenDevices = [];

        $from = 1;
        $to = $devices->count();

        $max = ['distance' => 0];

        for ($i = 0; $i < count($devices); $i++) {
            for ($j = $from; $j < $to; $j++) {
                $distance = $this->distanceBetweenTwoPoints($devices[$i]->latitude, $devices[$i]->longitude, $devices[$j]->latitude, $devices[$j]->longitude, 'K');
                $info = ['device1' => $devices[$i]->device_id, 'device2' => $devices[$j]->device_id, 'distance' => $distance];

                if ($max['distance'] < $distance) {
                    $max = $info;
                }

                $distanceBettweenDevices[] = $info;
            }
            $from++;
        }

        return ['distance_bettween_devices' => $distanceBettweenDevices, 'biggest_distance_bettween_devices' => $max];
    }

}