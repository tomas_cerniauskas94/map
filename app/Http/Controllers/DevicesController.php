<?php

namespace App\Http\Controllers;

use App\Models\Device;
use App\Service\DevicesService;
use Illuminate\Http\Request;
use Session;
use Mail;
use App\User;
use App\Mail\DeviceAdded;

class DevicesController extends Controller
{
    private $devicesService;

    public function __construct(DevicesService $devicesService)
    {
        $this->devicesService = $devicesService;
    }

    public function create()
    {
        return view('devices.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'device_id' => 'required|max:255|unique:devices,id',
            'gps_coords' => array(
                'required',
                'max:255',
                'regex:/^\-{1}?([0-9]+)|([0-9]+.[0-9]+),\-{1}?([0-9]+)|([0-9]+.[0-9]+)$/'
            ),
            'place' => 'required|in:1,2',
        ]);

        $coords = explode(',', $request->gps_coords);

        $lat = trim($coords[0]);
        $lng = trim($coords[1]);

        $country = null;
        $state = null;
        $city = null;
        $street = null;
        $houseNumber = null;

        $placeInfo = $this->devicesService->getPlaceInfoByCoords($lat, $lng);

        if (!empty($placeInfo['address'])) {
            if (!empty($placeInfo['address']['country'])) {
                $country = $placeInfo['address']['country'];
            }
            if (!empty($placeInfo['address']['state'])) {
                $state = $placeInfo['address']['state'];
            }
            if (!empty($placeInfo['address']['city'])) {
                $city = $placeInfo['address']['city'];
            }
            if (!empty($placeInfo['address']['road'])) {
                $street = $placeInfo['address']['road'];
            }
            if (!empty($placeInfo['address']['house_number'])) {
                $houseNumber = $placeInfo['address']['house_number'];
            }
        }

        $device = new Device;
        $device->device_id = $request->device_id;
        $device->latitude = $lat;
        $device->longitude = $lng;
        $device->place = $request->place;
        $device->country = $country;
        $device->state = $state;
        $device->city = $city;
        $device->street = $street;
        $device->house_number = $houseNumber;

        $device->save();

        if ($device->place == Device::PLACE_WORK) {
            $users = User::all();
            foreach ($users as $user) {
                Mail::to($user->email)->send(new DeviceAdded($device));
            }
        }

        Session::flash('success', 'Device was added.');

        return redirect()->route('devices.create');
    }
}
