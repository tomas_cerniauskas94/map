<?php

namespace App\Http\Controllers\Admin;

use App\Models\Device;
use App\Service\DevicesService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index(DevicesService $devicesService)
    {
        $devices = Device::orderBy('device_id')->get();

        $distanceBettweenDevices = $devicesService->getDistanceBettweenDevices($devices);

        return view('admin.admin.index', compact('devices', 'distanceBettweenDevices'));
    }
}
