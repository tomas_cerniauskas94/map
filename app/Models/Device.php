<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';
    protected $appends = ['place_name', 'full_address'];

    const PLACE_HOME = 1;
    const PLACE_WORK = 2;

    public function getPlaceNameAttribute()
    {
        switch($this->place) {
            case self::PLACE_HOME:
                return 'Home';
            case self::PLACE_WORK:
                return 'Work';
        }
    }

    public function getFullAddressAttribute()
    {
        $fullAddress = '';
        if(!empty($this->country)){
            $fullAddress .= $this->country;
        }
        if(!empty($this->state)){
            $fullAddress .= ' ,'.$this->state;
        }
        if(!empty($this->city)){
            $fullAddress .= ' ,'.$this->city;
        }
        if(!empty($this->street)){
            $fullAddress .= ' ,'.$this->street;
            if(!empty($this->house_number)){
                $fullAddress .= ' '.$this->house_number;
            }
        }
        return $fullAddress;
    }
}
