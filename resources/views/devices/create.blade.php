@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="device_id">Device ID</label>
                            <input id="device_id" name="device_id" class="form-control" value="{{ old('device_id') }}" placeholder="Device ID">
                            @if($errors->has('device_id'))
                                <span class="text-danger">{{ $errors->first('device_id') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="device_id">GPS coords</label>
                            <input id="device_id" name="gps_coords" class="form-control" value="{{ old('gps_coords') }}" placeholder="GPS coords, e.g.: 9.0200417, -79.5189333">
                            @if($errors->has('gps_coords'))
                                <span class="text-danger">{{ $errors->first('gps_coords') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="place">Place</label>
                            <select class="form-control" name="place">
                                <option value="">Select place</option>
                                <option value="{{ \App\Models\Device::PLACE_HOME }}" {{ old('place') == \App\Models\Device::PLACE_HOME ? 'selected':'' }}>Home</option>
                                <option value="{{ \App\Models\Device::PLACE_WORK }}" {{ old('place') == \App\Models\Device::PLACE_WORK ? 'selected':'' }}>Work</option>
                            </select>
                            @if($errors->has('place'))
                                <span class="text-danger">{{ $errors->first('place') }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
