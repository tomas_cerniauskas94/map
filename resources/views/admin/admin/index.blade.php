@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Devices
                </div>
                <div class="panel-body">
                    @foreach($devices as $device)
                        <div>{{ $device->device_id }}</div>
                    @endforeach
                    @if(!empty($distanceBettweenDevices['biggest_distance_bettween_devices']['distance']))
                        <hr>
                        <strong>Biggest distance between devices:</strong><br>
                        {{ $distanceBettweenDevices['biggest_distance_bettween_devices']['device1'] }} - {{ $distanceBettweenDevices['biggest_distance_bettween_devices']['device2'] }} - {{ $distanceBettweenDevices['biggest_distance_bettween_devices']['distance'] }} km
                    @endif

                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div id="map" style="height: 500px;">

            </div>
        </div>
    </div>
@endsection

@section('bottom_scripts')
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBv0CQB-Qj1ZKWT-N49_LsX13VIBzG-KKQ"></script>
    <script>
        var devices = JSON.parse('{!! $devices !!}');
        function Map(id){
            this.map = new google.maps.Map(document.getElementById(id));
            this.markers=[];
        }
        Map.prototype = {
            addMarkers: function(markers){
                var self = this;

                $.each(markers, function(index, value){
                    self.addMarker({
                        position: {
                            lat: value.latitude,
                            lng: value.longitude
                        },
                        map: self.map,
                        content: '<p><strong>Device ID:</strong> ' + value.device_id + '</p>' +
                        '<p><strong>Place:</strong> ' + value.place_name+ '</p>' +
                        '<p><strong>Address:</strong> '+value.full_address+'</p>'
                    });
                });

                var markerCluster = new MarkerClusterer(this.map, this.markers,
                    {imagePath: '/images/google_maps/m'});

                this._fitBounds();
            },
            addMarker: function(opts){
                var marker = new google.maps.Marker(opts);

                this._addMarkerToArray(marker);

                marker.addListener('click', function () {
                    //console.log(this);
                    var infowindow = new google.maps.InfoWindow({
                        content: opts.content
                    });
                    infowindow.open(map, marker);
                });
            },
            _addMarkerToArray: function(marker){
                this.markers.push(marker);
            },
            _fitBounds: function(){
                var bounds = new google.maps.LatLngBounds();
                if (this.markers.length > 0) {
                    for (var i = 0; i < this.markers.length; i++) {
                        console.log(this.markers[i].getPosition());
                        bounds.extend(this.markers[i].getPosition());
                    }
                    this.map.fitBounds(bounds);
                    this.map.panToBounds(bounds);
                }
            }
        };

        $(document).ready(function(){
            var map = new Map('map');
            map.addMarkers(devices);
        });
    </script>
@endsection